#include "image.h"

const struct image invalid_image = { .data = NULL, .width = 0, .height = 0 };

struct image new_image(uint32_t width, uint32_t height) {
    struct pixel* image_data = NULL;

    if (width > 0 && height > 0) {
        image_data = malloc(sizeof(struct pixel) * width * height);

        if (image_data == NULL) {
            return invalid_image;
        }
    }

    return (struct image) { .data = image_data, .width = width, .height = height };
}

int is_valid(struct image* img) {
    if (img == NULL || img->width == 0 || img->height == 0 || img->data == NULL) {
        return 0;
    }

    return 1;
}

void free_image(struct image* img) {
    if (img != NULL && img->data != NULL) {
        free(img->data);
    }
}
