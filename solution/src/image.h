#pragma once
#ifndef IMAGE_
#define IMAGE_

#include <stdint.h>
#include <stdlib.h>

struct pixel {
    uint8_t r, g, b;
};

struct image {
    uint32_t width;
    uint32_t height;
    struct pixel* data;
};

extern const struct image invalid_image;

struct image new_image(uint32_t width, uint32_t height);

int is_valid(struct image* img);

void free_image(struct image* img);
#endif
