#include "image.h"
#include "rotation.h"

struct image rotate(struct image* img) {
    if (!is_valid(img)) {
        return invalid_image;
    }

    struct image rotated_img = new_image(img->height, img->width);
    for (uint32_t i = 0; i < img->height; i++) {
        for (uint32_t j = 0; j < img->width; j++) {
            rotated_img.data[(j + 1) * img->height - i - 1] = img->data[i * img->width + j];
        }
    }

    return rotated_img;
}
