#pragma once
#ifndef BMP_IO_
#define BMP_IO_

#include "image.h"
#include <stdio.h>

#define BMP_SIGNATURE 0x4d42

#pragma pack(push, 1)
struct bmp_header
{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

/*  deserializer   */
enum read_status {
    READ_OK = 0x0,
    READ_ERROR
};

/*  serializer   */
enum write_status {
    WRITE_OK = 0x0,
    WRITE_ERROR = 0x2
};

enum read_status from_bmp(FILE* in, struct image* img);

enum write_status to_bmp(FILE* out, struct image const* img);
#endif
