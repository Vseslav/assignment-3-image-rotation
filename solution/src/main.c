#include "image.h"
#include "image_io.h"
#include "rotation.h"

int main( int argc, char** argv ) {
    if (argc != 3) {
        printf("Expected 3 arguments, but got %i", argc);
        return -1;
    }

    FILE* f_in = fopen(argv[1], "rb");
    if (f_in == NULL) {
        printf("Input file open error");
        return -1;
    }

    struct image img = new_image(0, 0);
    enum read_status read_status = from_bmp(f_in, &img);
    if (fclose(f_in) != 0) {
        printf("Input file close error");
        return -1;
    }
    if (read_status != READ_OK) {
        printf("%s = %d", "Read status", read_status);
        return -1;
    }

    FILE* f_out = fopen(argv[2], "wb");
    if (f_out == NULL) {
        printf("Output file open error");
        return -1;
    }

    struct image rotated_image = rotate(&img);
    free_image(&img);
    if (!is_valid(&rotated_image)) {
        printf("Rotated image is invalid");
        return -1;
    }

    enum write_status write_status = to_bmp(f_out, &rotated_image);
    free_image(&rotated_image);

    if (fclose(f_out) != 0) {
        printf("Output file close error");
        return -1;
    }
    if (write_status != WRITE_OK) {
        printf("%s = %d", "Write status", write_status);
        return -1;
    }

    return 0;
}
